package com.example.tour_guide;

public class PlacesOfInterest {

    private String placeName;
    private String placeAddress;
    private String placeSite;
    private String placeHours;
    private int placeImageID;
    private int placeCostTier;
    private String link;

    /***
     * Full constructor
     * @param placeName
     * @param placeAddress
     * @param placeSite
     * @param placeHours
     * @param placeImageID
     * @param placeCostTier
     * @param link
     */
    public PlacesOfInterest(String placeName, String placeAddress, String placeSite,
                            String placeHours, int placeImageID, int placeCostTier,
                            String link) {
        this.placeName = placeName;
        this.placeAddress = placeAddress;
        this.placeSite = placeSite;
        this.placeHours = placeHours;
        this.placeImageID = placeImageID;
        this.link = link;

        if (placeCostTier > 3) {
            this.placeCostTier = 3;
        }else if (placeCostTier < 0){
            this.placeCostTier = 0;
        }else{
            this.placeCostTier = placeCostTier;
        }

    }

    /***
     * Constructor for places to visit without any cost and no openHours
     * @param placeName
     * @param placeAddress
     * @param placeSite
     * @param placeImageID
     * @param link
     */
    public PlacesOfInterest(String placeName, String placeAddress, String placeSite,
                            int placeImageID, String link) {
        this.placeName = placeName;
        this.placeAddress = placeAddress;
        this.placeSite = placeSite;
        this.placeHours = "";
        this.placeImageID = placeImageID;
        this.placeCostTier = 0;
        this.link = link;
    }

    /***
     * For places whithout any cost but with opening hours (eg. city parks)
     * @param placeName
     * @param placeAddress
     * @param placeSite
     * @param placeHours
     * @param placeImageID
     * @param link
     */
    public PlacesOfInterest(String placeName, String placeAddress, String placeSite,
                            String placeHours, int placeImageID, String link) {
        this.placeName = placeName;
        this.placeAddress = placeAddress;
        this.placeSite = placeSite;
        this.placeHours = placeHours;
        this.placeImageID = placeImageID;
        this.placeCostTier = 0;
        this.link = link;
    }

    /***
     *   For places with no concrete openening hours (eg. theaters)
     * @param placeName
     * @param placeAddress
     * @param placeSite
     * @param placeImageID
     * @param placeCostTier
     * @param link
     */
    public PlacesOfInterest(String placeName, String placeAddress, String placeSite,
                            int placeImageID, int placeCostTier, String link) {
        this.placeName = placeName;
        this.placeAddress = placeAddress;
        this.placeSite = placeSite;
        this.placeHours = "";
        this.placeImageID = placeImageID;
        this.link = link;

        if (placeCostTier > 3) {
            this.placeCostTier = 3;
        }else if (placeCostTier < 0){
            this.placeCostTier = 0;
        }else{
            this.placeCostTier = placeCostTier;
        }
    }

    /***
     *
     * @return the name of the place
     */
    public String getPlaceName() {
        return this.placeName;
    }

    /***
     *
     * @return the address of the place (street and number, location in eg. city-center)
     */
    public String getPlaceAddress() {
        return placeAddress + " - " + placeSite;
    }

    public String getPlaceHours() {
        return placeHours;
    }

    /***
     *
     * @return the imageID of the place
     */
    public int getPlaceImageID() {
        return placeImageID;
    }

    /***
     *
     * @return the cost tier of the place (0 to 3)
     */
    public int getPlaceCostTier() {
        return placeCostTier;
    }

    /***
     *
     * @return the URL to the place site or wiki
     */
    public String getLink() {
        return link;
    }
}