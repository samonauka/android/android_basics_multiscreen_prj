package com.example.tour_guide;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import static com.example.tour_guide.R.layout.place_item;

public class PlaceAdapter extends ArrayAdapter<PlacesOfInterest> {

    public PlaceAdapter(Activity context, ArrayList<PlacesOfInterest> places){
        super(context,0,places);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItemView = convertView;

        if (listItemView == null){
            listItemView = LayoutInflater.from(getContext()).inflate(place_item, parent, false);
        }

        PlacesOfInterest currentPlace = getItem(position);

        PrepareView(listItemView, currentPlace);

        return listItemView;
    }

    public void PrepareView(View listItemView, final PlacesOfInterest currentPlace){


        View.OnClickListener goToLink = new View.OnClickListener() {
            public void onClick(View v){
                openWebURL(currentPlace.getLink());
            }
        };

        View layout = listItemView.findViewById(R.id.place_layout);
        layout.setOnClickListener(goToLink);

        TextView placeName = listItemView.findViewById(R.id.txt_place_1);
        int placeCostTier = currentPlace.getPlaceCostTier();


        if (placeCostTier == 1){
            placeName.setText(currentPlace.getPlaceName() + " - $");
        } else if (placeCostTier == 2){
            placeName.setText(currentPlace.getPlaceName() + " - $$");
        } else if (placeCostTier == 3) {
            placeName.setText(currentPlace.getPlaceName() + " - $$$");
        } else {
            placeName.setText(currentPlace.getPlaceName());
        }

        TextView placeHours = listItemView.findViewById(R.id.txt_place_3);
        placeHours.setText(currentPlace.getPlaceHours());


        TextView placeAddress = listItemView.findViewById(R.id.txt_place_2);
        placeAddress.setText(currentPlace.getPlaceAddress());

        ImageView placeImage = listItemView.findViewById(R.id.place_image);
        if (currentPlace.getPlaceImageID() > 0){
            placeImage.setImageResource(currentPlace.getPlaceImageID());
        }else{
            placeImage.setVisibility(View.GONE);
        }

    }

    public void openWebURL( String inURL ) {
        Intent browse = new Intent( Intent.ACTION_VIEW , Uri.parse( inURL ) );
        getContext().startActivity(browse);
    }
}
