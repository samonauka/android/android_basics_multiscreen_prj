package com.example.tour_guide;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class WalkInFragment  extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.list, container, false);

        final ArrayList<PlacesOfInterest> places = getArray();

        PlaceAdapter adapter = new PlaceAdapter(getActivity(), places);
        ListView listView = rootView.findViewById(R.id.list);

        listView.setAdapter(adapter);

        return rootView;
    }

    public ArrayList<PlacesOfInterest> getArray(){

        ArrayList<PlacesOfInterest> places = new ArrayList<>();

        places.add(new PlacesOfInterest("Piazza duomo",
                "Piazza del duomo", "Center",
                R.drawable.piazza_duomo,
                "https://en.wikipedia.org/wiki/Piazza_del_Duomo,_Milan"));

        places.add(new PlacesOfInterest("Piazza della scala",
                "Piazza della scala", "Center",
                R.drawable.piazza_della_scala,
                "https://en.wikipedia.org/wiki/Piazza_della_Scala"));

        places.add(new PlacesOfInterest("Piazza affari",
                "Piazza degli affari", "Center",
                R.drawable.piazza_affari,
                "https://it.wikipedia.org/wiki/Piazza_degli_Affari"));

        places.add(new PlacesOfInterest("Quartiere Brera",
                "Via Brera", "Brera",
                R.drawable.brera,
                "https://en.wikipedia.org/wiki/Brera_(district_of_Milan)"));

        places.add(new PlacesOfInterest("I Navigli",
                "Alzaia Naviglio Grande", "San Cristoforo Sul Naviglio",
                R.drawable.navigli,
                "https://en.wikipedia.org/wiki/Navigli"));

        places.add(new PlacesOfInterest("Quartiere Maggiolina",
                "Via Eugenio Torelli ,Viollier", "Maggiolina",
                R.drawable.maggiolina,
                "https://it.wikipedia.org/wiki/Maggiolina"));

        places.add(new PlacesOfInterest("Villa invernizzi",
                "Via Cappuccini 7", "Porta Venezia",
                "wen-sun: 10:00 - 17:15, mon-tue: Closed" ,
                R.drawable.villa_invernizzi,
                "https://www.tripadvisor.com/Attraction_Review-g187849-d12883218-Reviews-" +
                        "Villa_Invernizzi-Milan_Lombardy.html"));

        places.add(new PlacesOfInterest("Bosco verticale",
                "Via Gaetano de Castillia", "Isola",
                R.drawable.bosco_verticale,
                "https://en.wikipedia.org/wiki/Bosco_Verticale"));

        places.add(new PlacesOfInterest("Galleria Vittorio Emanuele",
                "Piazza del Duomo", "Center",
                R.drawable.vittorio_emanuele,
                "https://en.wikipedia.org/wiki/Galleria_Vittorio_Emanuele_II"));

        places.add(new PlacesOfInterest("Castello sforzesco",
                "Piazza castello", "Castello",
                "07:30 - 19:30",
                R.drawable.castello_sforzesco,
                "https://www.milanocastello.it/en"));

        places.add(new PlacesOfInterest("Arco della pace",
                "Piazza della pace", "Sempione",
                R.drawable.arco_della_pace,
                "https://en.wikipedia.org/wiki/Porta_Sempione"));

        places.add(new PlacesOfInterest("Orto botanico Brera",
                "Via Privata Fratelli Gabba", "Brera",
                "tue-sun: 10:00 - 18:00, mon: Closed",
                R.drawable.orto_botanico_brera,
                "https://en.wikipedia.org/wiki/Orto_Botanico_di_Brera"));

        places.add(new PlacesOfInterest("Parco Sempione",
                "Piazza Sempione", "Sempione",
                "06:30 - 21:00", R.drawable.parco_sempione,
                "https://en.wikipedia.org/wiki/Parco_Sempione"));

        return places;
    }
}
