package com.example.tour_guide;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import static com.example.tour_guide.R.layout.abc_activity_chooser_view_list_item;
import static com.example.tour_guide.R.layout.event_item;

public class EventAdapter  extends ArrayAdapter<RecurrentEvents> {

    public EventAdapter(Activity context, ArrayList<RecurrentEvents> events){
        super(context,0,events);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View listItemView = convertView;

        if (listItemView == null){
            listItemView = LayoutInflater.from(getContext()).inflate(event_item, parent, false);
        }

        final RecurrentEvents currentEvent = getItem(position);

        prepareView(listItemView, currentEvent);

        return listItemView;
    }

    public void prepareView(View listItemView, final RecurrentEvents currentEvent){

        View.OnClickListener goToLink = new View.OnClickListener() {
            public void onClick(View v){
                openWebURL(currentEvent.getLink());
            }
        };

        View layout = listItemView.findViewById(R.id.ev_layout);
        layout.setOnClickListener(goToLink);

        TextView eventName = listItemView.findViewById(R.id.txt_ev_1);
        int eventCostTier = currentEvent.getEventCostTier();

        switch(eventCostTier){
            case 1: eventName.setText(currentEvent.getEventName() +" - $");
            case 2: eventName.setText(currentEvent.getEventName() +" - $$");
            case 3: eventName.setText(currentEvent.getEventName() +" - $$$");
            default: eventName.setText(currentEvent.getEventName());
        }


        TextView eventLocation = listItemView.findViewById(R.id.txt_ev_2);
        eventLocation.setText(currentEvent.getEventLocation());

        TextView eventDateDuration = listItemView.findViewById(R.id.txt_ev_3);
        eventDateDuration.setText(currentEvent.getEventDuration());

        ImageView eventImage = listItemView.findViewById(R.id.ev_image);
        if (currentEvent.getImageId() > 0){
            eventImage.setImageResource(currentEvent.getImageId());
        }else{
            eventImage.setVisibility(View.GONE);
        }

    }

    public void openWebURL( String inURL ) {
        Intent browse = new Intent( Intent.ACTION_VIEW , Uri.parse( inURL ) );
        getContext().startActivity(browse);
    }
}
