package com.example.tour_guide;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class CultureFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.list, container, false);

        final ArrayList<PlacesOfInterest> places = getArray();

        PlaceAdapter adapter = new PlaceAdapter(getActivity(), places);
        ListView listView = rootView.findViewById(R.id.list);

        listView.setAdapter(adapter);

        return rootView;
    }

    public ArrayList<PlacesOfInterest> getArray(){

        ArrayList<PlacesOfInterest> places = new ArrayList<>();

        places.add(new PlacesOfInterest("Abbazia Chiaravalle",
                "Via Sant'Arialdo 102", "Milano Chiaravalle",
                "mon-sat: 09:00-12:30/15:00-18:30, sun: 11:00-12:30",
                R.drawable.abbazia_chiaravalle, 1,
                "https://en.wikipedia.org/wiki/Chiaravalle_Abbey"));

        places.add(new PlacesOfInterest("Binario 21",
                "Piazza Edmond Jacob Safra 1", "Central Station Area",
                "mon-wen/fri-sat: Closed, thu: 16:00-20:00, sun:10:00-17:00",
                R.drawable.binario_21, 1,
                "https://en.wikipedia.org/wiki/Memoriale_della_Shoah"));

        places.add(new PlacesOfInterest("Casa Manzoni",
                "Via Gerolamo Morone 1", "Scala",
                "tue-fri: 10:00-17:00, sat: 14:00-17:00, sun-mon: closed",
                R.drawable.casa_manzoni, 1,
                "https://www.casadelmanzoni.it/?language=en"));

        places.add(new PlacesOfInterest("Dialogo nel Buio",
                "Via vivaio 7", "Porte Monforte",
                "mon: 10:15-16:00, tue-fri: 09:45-16:00, sat: 14:00-20:45, sun: 13:00-18:00",
                R.drawable.dialogo_nel_buio, 2,
                "https://www.dialogonelbuio.org/"));

        places.add(new PlacesOfInterest("Duomo",
                "Piazza del Duomo", "Center",
                "09:00-19:00", R.drawable.duomo_milano, 3,
                "https://www.duomomilano.it/en/"));

        places.add(new PlacesOfInterest("La scala",
                "Via Filodrammatici 2", "Scala",
                R.drawable.la_scala, 3,
                "https://teatroallascala.org/en/index.html"));

        places.add(new PlacesOfInterest("Museo della Scienza e della Technica",
                "Via San Vittore 21", "San Vittore",
                "mon: closed, tue-fri: 09:30-17:00, sat-sun: 09:30-18:00",
                R.drawable.leonardo_da_vinci, 1,
                "https://www.museoscienza.org/en"));

        places.add(new PlacesOfInterest("Museo del 900",
                "Piazza del Duomo 8", "Center",
                "mon: closed, tue-wen/fri-sun: 09:30-18:30, thu: 09:30-21:30",
                R.drawable.museo_900, 1,
                "https://www.museodelnovecento.org/en/"));

        places.add(new PlacesOfInterest("Museo del Cenacolo",
                "Piazza di Santa Maria delle Grazie 2", "Zona magenta",
                "mon: closed, tue-sat: 09:00-18:45, sun: 09:00-13:30",
                R.drawable.museo_del_cenacolo, 2,
                "https://www.cenacolo.it/the-last-supper/"));

        places.add(new PlacesOfInterest("Museo del Risorgimento",
                "Via Borgonuovo 23", "Brera",
                "mon: closed, tue-sun: 09:00-13:00/14:00-17:30",
                R.drawable.museo_del_risorgimento, 0,
                "https://www.milanmuseumguide.com/museo-del-risorgimento/"));

        places.add(new PlacesOfInterest("Museo di storia naturale",
                "Corso Venezia 55", "Porta Venezia",
                "mon: closed, tue-sun: 09:00-17:30",
                R.drawable.museo_storia_naturale, 1,
                "https://www.milanmuseumguide.com/museo-storia-naturale/"));

        places.add(new PlacesOfInterest("Palazzo Morando",
                "Via Sant'Andrea 6", "Quadrilatero Della Moda",
                "mon: closed, tue-sun: 09:00-17:30",
                R.drawable.palazzo_morando, 0,
                "https://www.milanmuseumguide.com/palazzo-morando/"));

        places.add(new PlacesOfInterest("Pinacoteca di Brera",
                "Via Brera 28", "Brera",
                "mon: closed, tue-sun: 09:30-18:30",
                R.drawable.pinacoteca_brera, 0,
                "https://pinacotecabrera.org/en/"));

        return places;
    }
}
