package com.example.tour_guide;

public class RecurrentEvents {

    private String eventName;
    private String eventLocation;
    private String eventDuration;
    private String eventDate;
    private int imageId;
    private int eventCostTier;
    private String link;

    /***
     * Contructor for the event
     * @param eventName
     * @param eventLocation
     * @param eventDuration
     * @param eventDate
     * @param imageId
     * @param eventCostTier
     * @param link
     */
    public RecurrentEvents(String eventName, String eventLocation,
                           String eventDuration, String eventDate,
                           int imageId, int eventCostTier, String link) {
        this.eventName = eventName;
        this.eventLocation = eventLocation;
        this.eventDuration = eventDuration;
        this.eventDate = eventDate;
        this.imageId = imageId;
        this.link = link;

        if (eventCostTier > 3) {
            this.eventCostTier = 3;
        }else if (eventCostTier < 0){
            this.eventCostTier = 0;
        }else{
            this.eventCostTier = eventCostTier;
        }
    }

    /***
     *
     * @return the event name
     */
    public String getEventName() {
        return eventName;
    }

    /***
     *
     * @return the event Location (address or location eg. center)
     */
    public String getEventLocation() {
        return eventLocation;
    }

    /***
     *
     * @return the duration of the event and the date
     * (eg. start: 2020-10-01 duration: 2 days)
     */
    public String getEventDuration() {

        String date = "Start: " + this.eventDate;
        String duration = " Duration: " + this.eventDuration;

        return date + duration;
    }


    /***
     *
     * @return the event imageID
     */
    public int getImageId() {
        return imageId;
    }

    /***
     *
     * @return the event cost Tier (0 to 3)
     */
    public int getEventCostTier() {
        return eventCostTier;
    }

    /***
     *
     * @return the URL to the event site or wiki
     */
    public String getLink() {
        return link;
    }
}
