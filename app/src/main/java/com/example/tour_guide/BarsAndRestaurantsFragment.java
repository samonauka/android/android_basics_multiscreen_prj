package com.example.tour_guide;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class BarsAndRestaurantsFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.list, container, false);

        final ArrayList<PlacesOfInterest> places = getArray();

        PlaceAdapter adapter = new PlaceAdapter(getActivity(), places);
        ListView listView = rootView.findViewById(R.id.list);

        listView.setAdapter(adapter);

        return rootView;
    }

    public ArrayList<PlacesOfInterest> getArray(){

        ArrayList<PlacesOfInterest> places = new ArrayList<>();

        places.add(new PlacesOfInterest("La Birreria Italiana",
                "Piazzale Antonio Cantore 4", "Darsena",
                "mon-sat: 18:00-03:00, sun: 12:00-15:30/18:00-02:00",
                R.drawable.birreria_italian, 1,
                "https://www.tripadvisor.com/Restaurant_Review-g187849-d10218247-Reviews-La_Birreria_Italiana_Milano_Darsena-Milan_Lombardy.html"));

        places.add(new PlacesOfInterest("Caffe Napoli - Duomo",
                "Via Gaetano Giardino 1", "Cente,r",
                "mon-fri: 07:00-19:00, sat-sun: 08:00-18:00",
                R.drawable.caffe_napoli, 1,
                "https://www.tripadvisor.com/Restaurant_Review-g187849-d10270557-Reviews-Caffe_Napoli_Duomo-Milan_Lombardy.html"));

        places.add(new PlacesOfInterest("Crazy Cat Cafe",
                "Via Napo Torriani 5", "Central Station zone",
                "mon-sat:  10:00-20:30, sun: 10:00-20:00",
                R.drawable.crazy_cafe, 2,
                "https://www.tripadvisor.com/Restaurant_Review-g187849-d8769463-Reviews-Crazy_Cat_Cafe-Milan_Lombardy.html"));

        places.add(new PlacesOfInterest("Gente di mare",
                "Via vigevano 9", "Darsena",
                "mon-sat: 07:30-22:30, sun: 12:30-14:30/07:30-22:30",
                R.drawable.gente_di_mare, 3,
                "https://www.tripadvisor.com/Restaurant_Review-g187849-d19437309-Reviews-Gente_di_Mare_in_Darsena-Milan_Lombardy.html"));

        places.add(new PlacesOfInterest("GhePensi M.I.",
                "Piazza Morbegno 2", "Loreto",
                "mon: closed, tue-wen/sun: 17:00-01:30, fri-sat: 17:00-02:00",
                R.drawable.ghe_pensi_mi, 1,
                "https://www.tripadvisor.com/Restaurant_Review-g187849-d10780691-Reviews-GhePensi_M_I-Milan_Lombardy.html"));

        places.add(new PlacesOfInterest("Mono Bar",
                "Via Lecco 6", "Lazzaretto",
                "mon-thu/sun: 18:30-01:00, fri-sat: 18:30-02:00",
                R.drawable.mono_nero, 1,
                "https://www.tripadvisor.com/Restaurant_Review-g187849-d5211810-Reviews-Mono_Bar-Milan_Lombardy.html"));

        places.add(new PlacesOfInterest("Moscova quindici",
                "Via della Moscova 15", "Porta nuova",
                "mon-fri: 07:00-21:00, sun: closed",
                R.drawable.moscova_quindici, 2,
                "https://www.tripadvisor.com/Restaurant_Review-g187849-d4766076-Reviews-Moscova_quindici_milano-Milan_Lombardy.html"));

        places.add(new PlacesOfInterest("Orsonero Coffee",
                "Via Giuseppe Broggi 15", "Lazzaretto",
                "mon/sun: closed, tue-fri: 08:00-18:00",
                R.drawable.orsonero, 1,
                "https://www.tripadvisor.com/Restaurant_Review-g187849-d11929358-Reviews-Orsonero_Coffee-Milan_Lombardy.html"));

        places.add(new PlacesOfInterest("Panzarotti",
                "Viale Bligny 1/A", "Ticinese",
                "mon-thu/sat-sun: 12:00-15:30, fry: 12:00-15:30/18:30-21:30",
                R.drawable.panzarotti, 1,
                "https://www.tripadvisor.com/Restaurant_Review-g187849-d9996182-Reviews-Panzarotti-Milan_Lombardy.html"));

        places.add(new PlacesOfInterest("Piadineria Artigianale Pascoli",
                "Via Nicolo Paganini 2", "Buenos Aires",
                "mon-fri: 11:30-16:00/18:30-22:00, sat-sun: 11:30-16:00",
                R.drawable.piadineria_artigianale, 1,
                "https://www.tripadvisor.com/Restaurant_Review-g187849-d6091154-Reviews-Piadineria_Artigianale_Pascoli-Milan_Lombardy.html"));

        places.add(new PlacesOfInterest("Puccia&Pasta",
                "Via Trau' 2", "Isola",
                "mon: 09:00-23:00, tue-sun: 12:30-14:30/19:30-23:00",
                R.drawable.puccia_pasta, 2,
                "https://www.tripadvisor.com/Restaurant_Review-g187849-d17822713-Reviews-Puccia_Pasta-Milan_Lombardy.html"));

        places.add(new PlacesOfInterest("Il Sidro",
                "Via Federico Ozanam 15", "Buenos Aires",
                "18:00-02:00",
                R.drawable.sidro, 1,
                "https://www.tripadvisor.com/Restaurant_Review-g187849-d6879011-Reviews-Il_sidro-Milan_Lombardy.html"));

        return places;
    }
}
