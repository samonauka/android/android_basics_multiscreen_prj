package com.example.tour_guide;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class PageAdapter extends FragmentPagerAdapter {

    private Context myContext;

    public PageAdapter(Context context, FragmentManager fm){
        super(fm);
        myContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch(position){
            case 0: return new WalkInFragment();
            case 1: return new CultureFragment();
            case 2: return new BarsAndRestaurantsFragment();
            default: return new RecurrentEventsFragment();
        }
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {

        switch(position){
            case 0: return myContext.getString(R.string.categoryWalkIn);
            case 1: return myContext.getString(R.string.categoryCulture);
            case 2: return myContext.getString(R.string.categoryBarRestaurants);
            default: return myContext.getString(R.string.categoryEvents);
        }
    }
}
