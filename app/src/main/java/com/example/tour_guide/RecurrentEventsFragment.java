package com.example.tour_guide;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class RecurrentEventsFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.list, container, false);

        final ArrayList<RecurrentEvents> events = createArray();

                EventAdapter adapter = new EventAdapter(getActivity(), events);
        ListView listView = rootView.findViewById(R.id.list);
        listView.setAdapter(adapter);

        return rootView;
    }

    public ArrayList<RecurrentEvents> createArray() {

        final ArrayList<RecurrentEvents> events = new ArrayList<>();

        events.add(new RecurrentEvents("Milano Fashion Week Autumn/Winter",
                "streets of Milano", "7 Days",
                "September/October",
                R.drawable.autumn_winter, 0,
                "https://milanofashionweek.cameramoda.it/"));

        events.add(new RecurrentEvents("Carnevale Ambrosiano",
                "Milano City Center", "5 days",
                "The tuesday before the Lent",
                R.drawable.carnevale_ambrosiano, 0,
                "https://milanostyle.com/carnival-in-milan/"));

        events.add(new RecurrentEvents("Milano Coffee Festival",
                "Superstudio Piu, Via Tortona 27", "3 days",
                "15 May",
                R.drawable.coffee_festival, 2,
                "https://www.milancoffeefestival.com/"));

        events.add(new RecurrentEvents("Milano Film Festival",
                "via Santa Radegonda, 8", "7 days",
                "04 October",
                R.drawable.film_festival, 1,
               "http://www.milanofilmfestival.it/en/"));

        events.add(new RecurrentEvents("Milano Latin Festival",
                "Viale MilanoFiori - Assago", "66 days",
                "13 June",
                R.drawable.milano_latin_festival, 1,
                "https://www.milanolatinfestival.it/en"));

        events.add(new RecurrentEvents("Milano Pride",
                "Streets of Milano", "7 days",
                "end of June",
                R.drawable.milano_pride, 0,
                "http://www.milanopride.it/en/"));

        events.add(new RecurrentEvents("Museo City",
                "Museums of Milano", "3 days",
                "First weekend of March",
                R.drawable.museo_city, 1,
                "https://www.facebook.com/museocity/"));

        events.add(new RecurrentEvents("Milano Music Week",
                "Streets of Milano", "7 days",
                "16 November",
                R.drawable.music_week, 1,
                "https://www.milanomusicweek.it/en/"));

        events.add(new RecurrentEvents("Oh bej, Oh bej!",
                "From Piazza Castello to Piazza Gadio and Piazza Cannone",
                "4 days", "5 december",
                R.drawable.oh_bej, 2,
                "https://en.wikipedia.org/wiki/Oh_bej!_Oh_bej!"));

        events.add(new RecurrentEvents("Orticola per Milano",
                "Parks of Milano", "3 Days",
                "7 May",
                R.drawable.orticola, 1,
                "https://www.orticola.org/english/"));

        events.add(new RecurrentEvents("Milano Fashion Week Spring/Summer",
                "Streets of Milano", "7 days",
                "February/March",
                R.drawable.spring_summer, 0,
                "https://milanofashionweek.cameramoda.it/"));

        events.add(new RecurrentEvents("Stramilano",
                "Piazza Duomo 5/10km or Piazza Castello 21km", "1 day",
                "28 march",
                R.drawable.stramilano, 2,
                "https://www.stramilano.it/?lang=en"));

        return events;
    }
}
